import { ref } from "vue"; //, computed
import { defineStore } from "pinia";
import type Product from "@/types/Product";

export const useProductStore = defineStore("product", () => {
  const total = ref(0);
  // const doubleCount = computed(() => count.value * 2);
  const selectedProducts = ref(<Product[]>[]);
  const addItem = (id: number, name: string, price: number) => {
    selectedProducts.value.push({ id, name, price });
    total.value += price;
  };

  return { total, addItem, selectedProducts };
});
